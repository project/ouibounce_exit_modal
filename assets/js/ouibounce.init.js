(function (Drupal) {
  Drupal.ouibounce = Drupal.ouibounce || {
    instance: null,
    callbacks: {},
  };

  /**
   * Behavior to initialize Ouibounce.
   *
   * @type {{attach: Drupal.behaviors.initOuibounce.attach}}
   */
  Drupal.behaviors.initOuibounce = {
    attach(context, settings) {
      once(
        'ouibounce',
        document.getElementById('ouibounce-exit-modal'),
        context,
      ).forEach(function (item) {
        if (typeof ouibounce !== 'function') {
          console.error(
            'Ouibounce library is not present. Please require it (preferably via `composer require bower-asset/ouibounce`)',
          );
          return;
        }
        const config = settings.ouibounce;

        // Register close triggers
        document.querySelectorAll('.ouibounce-close').forEach(() => {
          item.addEventListener('click', (event) => {
            item.style.display = 'none';
            item.classList.remove('ouibounce-exit-modal--visible');
            item.classList.add('ouibounce-exit-modal--hidden');
          });
        });

        Drupal.ouibounce.instance = ouibounce(item, {
          sensitivity: config.sensitivity,
          aggressive: config.aggressive_mode,
          timer: config.timer,
          delay: config.delay,
          cookieExpire: config.cookie_expiration,
          cookieDomain: config.cookie_domain,
          cookieName: config.cookie_name,
          sitewide: config.sitewide_cookie,
          callback() {
            item.classList.add('ouibounce-exit-modal--visible');
          },
        });

        if (parseInt(config.custom_behavior) > 0) {
          Drupal.ouibounce.callbacks.initCustomBehavior(config);
        }
      });
    },
  };

  /**
   * Callback function to detect touch screen device.
   *
   * @return {boolean}
   *   Whether touch is enabled.
   */
  Drupal.ouibounce.callbacks.isTouchEnabled = function () {
    return (
      'ontouchstart' in window ||
      navigator.maxTouchPoints > 0 ||
      navigator.msMaxTouchPoints > 0
    );
  };

  /**
   * Callback function to initialize custom Ouibounce behavior.
   *
   * @param config
   *   The Ouibounce configuration.
   */
  Drupal.ouibounce.callbacks.initCustomBehavior = function (config) {
    const onlyOnTouchDevice = parseInt(config.custom_behavior_touch || 0);
    if (onlyOnTouchDevice && !Drupal.ouibounce.callbacks.isTouchEnabled()) {
      return;
    }

    switch (parseInt(config.custom_behavior)) {
      case 1:
        let fired = false;
        let scrollY = 0;
        window.addEventListener('scroll', function (e) {
          if (!fired && scrollY - window.scrollY > 0) {
            Drupal.ouibounce.instance.fire();
            fired = true;
          }
          scrollY = window.scrollY;
        });
        break;

      case 2:
        setTimeout(function () {
          Drupal.ouibounce.instance.fire();
        }, parseInt(config.custom_behavior_delay));
        break;
    }
  };
})(Drupal);
