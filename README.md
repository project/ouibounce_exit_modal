# Ouibounce Exit Modal
With this drupal module, you can embed any kind of block into the Ouibounce
modal window.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/ouibounce_exit_modal).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/ouibounce_exit_modal).

## Table of contents
- Requirements
- Installation
- Tips
- Maintainers

## Requirements
This module requires the [ouibounce](https://github.com/carlsednaoui/ouibounce)
library.

## Installation
Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

### With composer and asset-packagist
Run `composer require bower-asset/ouibounce`

### Manual installation
- Download Ouibounce library from https://github.com/carlsednaoui/ouibounce
- Place the folder inside the "/libraries" folder

## Tips
### Triggering the modal via JS / browser console

Sometimes you'd like to trigger the modal via JS or from the browser console for
styling. This can be achieved by the following call:

~~~
ouibounce(document.getElementById('ouibounce-exit-modal')).fire();
~~~

## Maintainers
- Sándor Juhász - [lonalore](https://www.drupal.org/u/lonalore)
- Julian Pustkuchen - [anybody](https://www.drupal.org/u/anybody)
- Joshua Sedler - [grevil](https://www.drupal.org/u/grevil)
