<?php

namespace Drupal\ouibounce_exit_modal\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;

/**
 * Provides a block.
 *
 * @Block(
 *  id = "ouibounce_block",
 *  admin_label = @Translation("Ouibounce Block"),
 * )
 */
class OuibounceBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $values = [
      'body_type'             => 0,
      'body'                  => '',
      'footer'                => 'Close',
      'sensitivity'           => 20,
      'aggressive_mode'       => 0,
      'modal_timer'           => 1000,
      'delay'                 => 0,
      'cookie_expiration'     => 0,
      'cookie_domain'         => '',
      'cookie_name'           => 'viewedOuibounceModal',
      'sitewide_cookie'       => 0,
      'custom_behavior'       => 0,
      'custom_behavior_delay' => 5000,
      'custom_behavior_touch' => 0,
    ];

    return $values;
  }

  /**
   * {@inheritdoc}
   */
  protected function baseConfigurationDefaults() {
    $config = parent::baseConfigurationDefaults();
    $config['label_display'] = 'invisible';
    return $config;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['label_display']['#access'] = FALSE;
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $formState) {
    $form['content'] = [
      '#type'        => 'fieldset',
      '#title'       => $this->t('Content settings'),
      '#collapsible' => TRUE,
      '#collapsed'   => FALSE,
    ];

    $form['content']['body_type'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Block content'),
      '#description'   => $this->t('Select which content to render in the modal. You may enter custom HTML content or select a block.
        Please note, that blocks must exist in the default theme to be selectable here. You may put them into a hidden region for example.'),
      '#options'       => ouibounce_exit_modal_get_blocks(),
      '#default_value' => $this->configuration['body_type'],
    ];

    $form['content']['body'] = [
      '#type'          => 'textarea',
      '#title'         => $this->t('Custom HTML body content'),
      '#default_value' => $this->configuration['body'],
      '#states'        => [
        'visible' => [
          'select[name="settings[content][body_type]"]' => ['value' => 0],
        ],
      ],
    ];

    $form['content']['footer'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Modal footer "Close" text'),
      '#default_value' => $this->configuration['footer'],
    ];

    $form['ouibounce'] = [
      '#type'        => 'fieldset',
      '#title'       => $this->t('Ouibounce JavaScript settings'),
      '#collapsible' => TRUE,
      '#collapsed'   => FALSE,
    ];

    $form['ouibounce']['sensitivity'] = [
      '#type'             => 'number',
      '#title'            => $this->t('Sensitivity'),
      '#default_value'    => (int) $this->configuration['sensitivity'],
      '#description'      => $this->t('Ouibounce fires when the mouse cursor moves close to (or passes) the top of the viewport. You can define how far the mouse has to be before Ouibounce fires. The higher value, the more sensitive, and the more quickly the event will fire. Defaults to 20.'),
      '#size'             => 5,
    ];

    $form['ouibounce']['aggressive_mode'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Aggressive mode'),
      '#options'       => [
        1 => $this->t('Enabled'),
        0 => $this->t('Disabled'),
      ],
      '#default_value' => (int) $this->configuration['aggressive_mode'],
      '#description'   => $this->t('By default, Ouibounce will only fire once for each visitor. When Ouibounce fires, a cookie is created to ensure a non obtrusive experience. There are cases, however, when you may want to be more aggressive (as in, you want the modal to be elegible to fire anytime the page is loaded/ reloaded). An example use-case might be on your paid landing pages. If you enable aggressive, the modal will fire any time the page is reloaded, for the same user.'),
    ];

    $form['ouibounce']['modal_timer'] = [
      '#type'             => 'number',
      '#title'            => $this->t('Set a min time before Ouibounce fires'),
      '#default_value'    => (int) $this->configuration['modal_timer'],
      '#description'      => $this->t("By default, Ouibounce won't fire in the first second to prevent false positives, as it's unlikely the user will be able to exit the page within less than a second. If you want to change the amount of time that firing is surpressed for, you can pass in a number of milliseconds to timer."),
      '#size'             => 5,
      '#field_suffix'     => $this->t('millisecond(s)'),
    ];

    $form['ouibounce']['delay'] = [
      '#type'             => 'number',
      '#title'            => $this->t('Delay'),
      '#default_value'    => (int) $this->configuration['delay'],
      '#description'      => $this->t("By default, Ouibounce will show the modal immediately. You could instead configure it to wait x milliseconds before showing the modal. If the user's mouse re-enters the body before delay ms have passed, the modal will not appear. This can be used to provide a 'grace period' for visitors instead of immediately presenting the modal window."),
      '#size'             => 5,
      '#field_suffix'     => $this->t('millisecond(s)'),
    ];

    $form['ouibounce']['cookie_expiration'] = [
      '#type'             => 'number',
      '#title'            => $this->t('Cookie expiration'),
      '#default_value'    => (int) $this->configuration['cookie_expiration'],
      '#description'      => $this->t("Ouibounce sets a cookie by default to prevent the modal from appearing more than once per user. You can add a cookie expiration (in days) using cookieExpire to adjust the time period before the modal will appear again for a user. By default, the cookie will expire at the end of the session, which for most browsers is when the browser is closed entirely."),
      '#size'             => 5,
      '#field_suffix'     => $this->t('day(s)'),
    ];

    $form['ouibounce']['cookie_domain'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Cookie domain'),
      '#default_value' => $this->configuration['cookie_domain'],
      '#description'   => $this->t('Ouibounce sets a cookie by default to prevent the modal from appearing more than once per user. You can add a cookie domain using cookieDomain to specify the domain under which the cookie should work. By default, no extra domain information will be added. If you need a cookie to work also in your subdomain (like blog.example.com and example.com), then set a cookieDomain such as .example.com (notice the dot in front).'),
    ];

    $form['ouibounce']['cookie_name'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Cookie name'),
      '#default_value' => $this->configuration['cookie_name'],
      '#description'   => $this->t('You can specify custom cookie name.'),
    ];

    $form['ouibounce']['sitewide_cookie'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Sitewide cookie'),
      '#options'       => [
        1 => $this->t('Enabled'),
        0 => $this->t('Disabled'),
      ],
      '#default_value' => (int) $this->configuration['sitewide_cookie'],
      '#description'   => $this->t('You can drop sitewide cookies by enabling this.'),
    ];

    $form['ouibounce_custom'] = [
      '#type'        => 'fieldset',
      '#title'       => $this->t('Custom Ouibounce behavior settings'),
      '#description' => $this->t('The behaviors that can be selected here are not part of the Ouibounce library, but supplement the original functionality.'),
      '#collapsible' => TRUE,
      '#collapsed'   => FALSE,
    ];

    $form['ouibounce_custom']['custom_behavior'] = [
      '#type'          => 'radios',
      '#title'         => $this->t('Custom behavior'),
      '#options'       => [
        0 => $this->t('No custom behavior'),
        1 => $this->t('Triggering when scrolled up'),
        2 => $this->t('Delayed triggering'),
      ],
      0 => [
        '#description' => $this->t('Work as configured above.'),
      ],
      1 => [
        '#description' => $this->t('Ouibounce will show the modal when the page is scrolled up.'),
      ],
      2 => [
        '#description' => $this->t('Ouibounce will show the modal after x milliseconds.'),
      ],
      '#default_value' => (int) $this->configuration['custom_behavior'],
    ];

    $form['ouibounce_custom']['custom_behavior_delay'] = [
      '#type'             => 'number',
      '#title'            => $this->t('Delay'),
      '#default_value'    => (int) $this->configuration['custom_behavior_delay'],
      '#size'             => 5,
      '#field_suffix'     => $this->t('millisecond(s)'),
      '#states' => [
        'visible' => [
          'input[name="settings[ouibounce_custom][custom_behavior]"]' => [
            'value' => '2',
          ],
        ],
      ],
    ];

    $form['ouibounce_custom']['custom_behavior_touch'] = [
      '#type'             => 'checkbox',
      '#title'            => $this->t('Use custom behavior only on touch screen devices'),
      '#default_value'    => !empty($this->configuration['custom_behavior_touch']),
      '#states' => [
        'invisible' => [
          'input[name="settings[ouibounce_custom][custom_behavior]"]' => [
            'value' => '0',
          ],
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $this->configuration['body_type'] = $values['content']['body_type'];
    $this->configuration['body'] = $values['content']['body'];
    $this->configuration['footer'] = $values['content']['footer'];
    $this->configuration['sensitivity'] = (int) $values['ouibounce']['sensitivity'];
    $this->configuration['aggressive_mode'] = (int) $values['ouibounce']['aggressive_mode'];
    $this->configuration['modal_timer'] = (int) $values['ouibounce']['modal_timer'];
    $this->configuration['delay'] = (int) $values['ouibounce']['delay'];
    $this->configuration['cookie_expiration'] = (int) $values['ouibounce']['cookie_expiration'];
    $this->configuration['cookie_domain'] = $values['ouibounce']['cookie_domain'];
    $this->configuration['cookie_name'] = $values['ouibounce']['cookie_name'];
    $this->configuration['sitewide_cookie'] = (int) $values['ouibounce']['sitewide_cookie'];
    $this->configuration['custom_behavior'] = (int) $values['ouibounce_custom']['custom_behavior'];
    $this->configuration['custom_behavior_delay'] = (int) $values['ouibounce_custom']['custom_behavior_delay'];
    $this->configuration['custom_behavior_touch'] = (int) $values['ouibounce_custom']['custom_behavior_touch'];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $id = $this->configuration['body_type'];

    if (empty($id)) {
      $content = Markup::create($this->configuration['body']);
    }
    else {
      $content = ouibounce_exit_modal_render_block($id);
    }

    $build = [
      '#theme'    => 'ouibounce_block',
      '#title'    => $this->checkPlain($this->label()),
      '#footer'   => $this->checkPlain($this->configuration['footer']),
      '#body'     => $content,
      '#attached' => [
        'library'        => [
          'ouibounce_exit_modal/ouibounce.init',
        ],
        'drupalSettings' => [
          'ouibounce' => [
            'sensitivity'           => (int) $this->configuration['sensitivity'],
            'aggressive_mode'       => (bool) $this->configuration['aggressive_mode'],
            'timer'                 => (int) $this->configuration['modal_timer'],
            'delay'                 => (int) $this->configuration['delay'],
            'cookie_expiration'     => (int) $this->configuration['cookie_expiration'],
            'cookie_domain'         => $this->checkPlain($this->configuration['cookie_domain']),
            'cookie_name'           => $this->checkPlain($this->configuration['cookie_name']),
            'sitewide_cookie'       => (bool) $this->configuration['sitewide_cookie'],
            'custom_behavior'       => (int) $this->configuration['custom_behavior'],
            'custom_behavior_delay' => (int) $this->configuration['custom_behavior_delay'],
            'custom_behavior_touch' => (int) $this->configuration['custom_behavior_touch'],
          ],
        ],
      ],
    ];

    return $build;
  }

  /**
   * Encodes special characters in a plain-text string for display as HTML.
   *
   * @param string $text
   *   The text to be checked or processed.
   *
   * @return string
   *   An HTML safe version of $text.
   */
  public function checkPlain($text) {
    return htmlspecialchars($text, ENT_QUOTES, 'UTF-8');
  }

}
