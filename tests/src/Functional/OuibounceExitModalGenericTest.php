<?php

namespace Drupal\Tests\ouibounce_exit_modal\Functional;

use Drupal\Tests\system\Functional\Module\GenericModuleTestBase;

/**
 * Generic module test for ouibounce_exit_modal.
 *
 * @group ouibounce_exit_modal
 */
class OuibounceExitModalGenericTest extends GenericModuleTestBase {

  /**
   * {@inheritDoc}
   */
  protected function assertHookHelp(string $module): void {
    // Don't do anything here. We intend to implement hook_help() differently.
  }

}
